import psutil
import matplotlib.pyplot as plt
from Processes import processes, make_rand_process
from FCFS_SJF import calc_wait_times, calc_turn_around_times, \
    calc_completion_times, calc_average_times, plot_guntt 


def do_fcfs(processes):
    sorted_by_arrive = sorted(processes, key=lambda x: x.arrival_time)

    fcfs_avg_wait, fcfs_avg_turn_around, fcfs_avg_completion = calc_average_times(sorted_by_arrive)

    print('-------------------------------------FCFS--------------------------------------------')
    for item in sorted_by_arrive:
        print(item.get_info())

    fcfs_cpu = psutil.cpu_percent()
    print('FCFS cpu utilization = {}'.format(fcfs_cpu))
    print('FCFS average waiting time = {}'.format(fcfs_avg_wait))
    print('FCFS average turn around time = {}'.format(fcfs_avg_turn_around))
    print('FCFS average completion time = {}'.format(fcfs_avg_completion))
    
    plot_guntt(sorted_by_arrive, 'fcfs.png', 'FCFS')

    return(fcfs_avg_wait, fcfs_avg_turn_around, fcfs_avg_completion, fcfs_cpu)


if __name__ == "__main__":
    num = int(input('Enter number of processes: '))
    do_fcfs(make_rand_process(num))