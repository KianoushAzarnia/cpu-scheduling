from Processes import processes, make_rand_process
import psutil
import matplotlib.pyplot as plt
from FCFS import do_fcfs
from SJF import do_sjf
from RoundRobin import do_RR 

num = int(input('Enter number of processes: '))
processes = make_rand_process(num)
quantum_time = int(input('Enter the quantum time: '))

fcfs_avg_wait, fcfs_avg_turn_around, fcfs_avg_completion, fcfs_cpu = do_fcfs(processes)
sjf_avg_wait, sjf_avg_turn_around, sjf_avg_completion, sjf_cpu = do_sjf(processes)
rr_avg_wait, rr_avg_turn_around, rr_avg_completion, rr_cpu = do_RR(processes, quantum_time)

plt.plot(['FCFS','SJF','RR'],[fcfs_avg_wait, sjf_avg_wait, rr_avg_wait])
plt.ylabel('Average Waiting Times')
plt.xlabel('policy')
plt.show()

plt.plot(['FCFS','SJF','RR'],[fcfs_avg_turn_around, sjf_avg_turn_around, rr_avg_turn_around])
plt.ylabel('average turn around times')
plt.xlabel('policy')
plt.show()

plt.plot(['FCFS','SJF','RR'],[fcfs_avg_completion, sjf_avg_completion, rr_avg_completion])
plt.ylabel('average completion times')
plt.xlabel('policy')
plt.show()


plt.plot(['FCFS','SJF','RR'],[fcfs_cpu, sjf_cpu, rr_cpu])
plt.ylabel('cpu utilization')
plt.xlabel('policy')
plt.show()

