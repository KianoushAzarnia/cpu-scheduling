import psutil
import matplotlib.pyplot as plt
from Processes import processes, make_rand_process
from FCFS_SJF import calc_wait_times, calc_turn_around_times, \
    calc_completion_times, calc_average_times, plot_guntt 


def sjf_sort(sorted_by_sth):
    pool = sorted_by_sth
    pool[0].completion_time = pool[0].arrival_time + pool[0].burst_time

    for i in range(1, len(pool)):
        time = pool[i-1].completion_time

        # partial sorting  <-------------- right here !!!!
        waitings = list(filter(lambda x: x.arrival_time <= time, pool[i:]))
        pool[i:i+len(waitings)] = sorted(waitings, key=lambda x: x.burst_time)
        # partial sorting end

        if pool[i-1].completion_time < pool[i].arrival_time:
            pool[i].completion_time = pool[i-1].completion_time +\
                pool[i].burst_time + pool[i].arrival_time - pool[i-1].completion_time
        else:
            pool[i].completion_time = pool[i-1].completion_time + pool[i].burst_time
    return pool


def do_sjf(processes):
    sorted_by_arrive_burst = sorted(processes, key=lambda x: (x.arrival_time, x.burst_time))
    # print(len(processes))
    # for item in sorted_by_arrive_burst:
    #     print(item.get_info())

    pool = sjf_sort(sorted_by_arrive_burst)
    print('---------------------------------------SJF---pool-------------------------------------------')
    for item in pool:
        print(item.get_info())
    # print(len(pool))

    sjf_avg_wait, sjf_avg_turn_around, sjf_avg_completion = calc_average_times(sorted_by_arrive_burst)
    sjf_cpu = psutil.cpu_percent()

    print('SJF cpu utilization = {}'.format(sjf_cpu))
    print('SJF average waiting time = {}'.format(sjf_avg_wait))
    print('SJF average turn around time = {}'.format(sjf_avg_turn_around))
    print('SJF average completion time = {}'.format(sjf_avg_completion))
    print('---------------------------------------------------------')

    plot_guntt(sorted_by_arrive_burst, 'sjf.png', 'SJF')

    return(sjf_avg_wait, sjf_avg_turn_around, sjf_avg_completion, sjf_cpu)


if __name__ == "__main__":
    num = int(input('Enter number of processes: '))
    do_sjf(make_rand_process(num))