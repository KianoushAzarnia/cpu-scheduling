import matplotlib.pyplot as plt 


def calc_wait_times(sorted_by_sth):

    sorted_by_sth[0].response_time = 0
    sorted_by_sth[0].waiting_time = 0

    
    for i in range(1, len(sorted_by_sth)):
        sorted_by_sth[i].response_time = \
            sorted_by_sth[i-1].response_time + sorted_by_sth[i-1].burst_time
        sorted_by_sth[i].waiting_time = \
            max(0, sorted_by_sth[i].response_time - sorted_by_sth[i].arrival_time)


def calc_turn_around_times(sorted_by_sth):

    for i in range(1, len(sorted_by_sth)):
        sorted_by_sth[i].turn_around_time = sorted_by_sth[i].burst_time +\
            sorted_by_sth[i].waiting_time


def calc_completion_times(sorted_by_sth):

    for i in range(1, len(sorted_by_sth)):
        sorted_by_sth[i].completion_time = sorted_by_sth[i].turn_around_time +\
            sorted_by_sth[i].arrival_time


# average of waiting and turn around times
def calc_average_times(sorted_by_sth):

    calc_wait_times(sorted_by_sth)
    calc_turn_around_times(sorted_by_sth)
    calc_completion_times(sorted_by_sth)

    sum_of_wait_times = 0
    sum_of_turn_around_times = 0
    sum_of_completion_times = 0

    for i in range(len(sorted_by_sth)):
        sum_of_wait_times += sorted_by_sth[i].waiting_time
        sum_of_turn_around_times += sorted_by_sth[i].turn_around_time
        sum_of_completion_times += sorted_by_sth[i].completion_time


    average_wait_time = sum_of_wait_times / len(sorted_by_sth)
    average_turn_around_time = sum_of_turn_around_times / len(sorted_by_sth)
    average_completion_time = sum_of_completion_times / len(sorted_by_sth)

    return (average_wait_time, average_turn_around_time, average_completion_time)


def plot_guntt(processes, file_name, figure_title = None):
    fig, gnt = plt.subplots() 
    if figure_title is not None:
        gnt.set_title(figure_title)
    else:
        gnt.set_title(file_name)
    gnt.set_xlabel('seconds since start') 
    gnt.set_ylabel('Process')
    gnt.set_yticks([i for i in range(1, len(processes)+1)])
    gnt.set_yticklabels(['{}'.format(i) for i in range(1, len(processes)+1)])
    for proc in processes:
        gnt.broken_barh([(proc.response_time, proc.burst_time)], (proc.process_id, 1))
    plt.savefig(file_name)
    plt.show()