import psutil
import matplotlib.pyplot as plt
from Processes import processes, make_rand_process

def schudle_RR(processes, quantum_time=5):
    my_quantum = int(quantum_time)
    proc_list = sorted(processes, key=lambda x: x.arrival_time)
    time = 0
    is_finished_process = [False] * len(proc_list)
    sorted_RR_finished = []

    while not all(is_finished_process):
        for i in range(len(proc_list)):
            if proc_list[i].next_arrival_time <= time:
                if not is_finished_process[i]:
                    if proc_list[i].next_arrival_time <= my_quantum:
                        if proc_list[i].remaining_burst_time > my_quantum:
                            time += my_quantum
                            proc_list[i].remaining_burst_time -= my_quantum
                            proc_list[i].next_arrival_time += my_quantum
                            proc_list[i].save(time, my_quantum)
                        else:
                            time += proc_list[i].remaining_burst_time
                            ### calculating parameters
                            proc_list[i].waiting_time = time - \
                                (proc_list[i].next_arrival_time + proc_list[i].remaining_burst_time)
                            proc_list[i].turn_around_time = proc_list[i].waiting_time + proc_list[i].burst_time
                            proc_list[i].completion_time = \
                                proc_list[i].turn_around_time + proc_list[i].arrival_time
                            ### if remaining burt time is zero then the process is finished
                            proc_list[i].remaining_burst_time = 0
                            is_finished_process[i] = True
                            proc_list[i].save(time)
                            sorted_RR_finished.append(proc_list[i])
                    elif proc_list[i].next_arrival_time > my_quantum:
                        for j in range(len(proc_list)):
                            if proc_list[j].next_arrival_time < proc_list[i].next_arrival_time:
                                if proc_list[j].remaining_burst_time > my_quantum:
                                    time += my_quantum
                                    proc_list[j].remaining_burst_time -= my_quantum
                                    proc_list[j].next_arrival_time += my_quantum
                                    proc_list[j].save(time, my_quantum)
                                else:
                                    time += proc_list[j].remaining_burst_time
                                    ### calculating parameters
                                    proc_list[j].waiting_time = time - \
                                        (proc_list[j].next_arrival_time + proc_list[j].remaining_burst_time)
                                    proc_list[j].turn_around_time = \
                                        proc_list[j].waiting_time + proc_list[j].burst_time
                                    proc_list[j].completion_time = \
                                        proc_list[j].turn_around_time + proc_list[j].arrival_time
                                    ### if remaining burt time is zero then the process is finished
                                    proc_list[j].remaining_burst_time = 0
                                    is_finished_process[j] = True
                                    proc_list[j].save(time)
                                    if proc_list[j] not in sorted_RR_finished:
                                        sorted_RR_finished.append(proc_list[j])
                        if not is_finished_process[i]:
                            if proc_list[i].remaining_burst_time > my_quantum:
                                time += my_quantum
                                proc_list[i].remaining_burst_time -= my_quantum
                                proc_list[i].next_arrival_time += my_quantum
                                proc_list[i].save(time, my_quantum)
                            else:
                                time += proc_list[i].remaining_burst_time
                                ### calculating parameters
                                proc_list[i].waiting_time = time - \
                                    (proc_list[i].next_arrival_time + proc_list[i].remaining_burst_time)
                                proc_list[i].turn_around_time = \
                                    proc_list[i].waiting_time + proc_list[i].burst_time
                                proc_list[i].completion_time = \
                                    proc_list[i].turn_around_time + proc_list[i].arrival_time
                                ### if remaining burt time is zero then the process is finished
                                proc_list[i].remaining_burst_time = 0
                                is_finished_process[i] = True
                                proc_list[i].save(time)
                                sorted_RR_finished.append(proc_list[i])
            else:
                time += 1
                i -= 1  

    return sorted_RR_finished


def guntt_RR(processes, file_name, figure_title = None):
    fig, gnt = plt.subplots() 
    if figure_title is not None:
        gnt.set_title(figure_title)
    else:
        gnt.set_title(file_name)
    gnt.set_xlabel('seconds since start') 
    gnt.set_ylabel('Process')
    gnt.set_yticks([i for i in range(1, len(processes)+1)])
    gnt.set_yticklabels(['{}'.format(i) for i in range(1, len(processes)+1)])
    for proc in processes:
        for a_tuple in proc.log:
            start, exe_time = a_tuple
            gnt.broken_barh([(start, exe_time)], (proc.process_id+1, 1))
    plt.savefig(file_name)
    plt.show()

    
def do_RR(processes, quantum_time): 
    sorted_RR_finished = schudle_RR(processes, quantum_time)

    sum_of_wait_times = 0
    sum_of_turn_around_times = 0
    sum_of_completion_times = 0

    print('------------------------------------After-----RR-------------------------------------------')
    for proc in sorted_RR_finished:
        sum_of_wait_times += proc.waiting_time
        sum_of_turn_around_times += proc.turn_around_time
        sum_of_completion_times += proc.completion_time
        print(proc.get_info())

    num = len(processes)
    rr_avg_wait, rr_avg_turn_around, rr_avg_completion = \
        sum_of_wait_times/num, sum_of_turn_around_times/num, sum_of_completion_times/num        


    rr_cpu = psutil.cpu_percent()
    print('Round Robin cpu utilization = {}'.format(rr_cpu))
    print('Round Robin average waiting time = {}'.format(rr_avg_wait))
    print('Round Robin average turn around time = {}'.format(rr_avg_turn_around))
    print('Round Robin average completion time = {}'.format(rr_avg_completion))
    print('---------------------------------------------------------')

    guntt_RR(sorted_RR_finished, 'RR.png', 'RR')

    return(rr_avg_wait, rr_avg_turn_around, rr_avg_completion, rr_cpu)


if __name__ == "__main__":
    num = int(input('Enter number of processes: '))
    quantum_time = int(input('Enter the quantum time: '))
    processes = make_rand_process(num)
    do_RR(processes, quantum_time)