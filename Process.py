class my_process:
    def __init__(self, process_id, burst_time, arrival_time, priority=1, criticals=[]):
        self.process_id = process_id
        self.burst_time = burst_time
        self.arrival_time = arrival_time
        self.priority = priority
        self.completion_time = burst_time
        self.turn_around_time = 0
        self.remaining_burst_time = burst_time
        self.next_arrival_time = arrival_time
        self.response_time = 0
        self.waiting_time = 0
        self.criticals = criticals
        self.log = []


    def get_info(self):
        return 'process id: {}, arrival time: {}, burst time: {}, priority: {}, response time: {}'\
            .format(self.process_id, self.arrival_time, self.burst_time, self.priority, self.response_time)


    def save(self,start_time, exe_time=0):
        if exe_time == 0:
            exe_time = self.remaining_burst_time
        self.log.append((start_time, exe_time))


    def report_exe(self):
        if self.remaining_burst_time > 0:
            status = 'not finished'
        else:
            status = 'finished'
        print('process with id: {} {}.'.format(self.process_id(), status))
