import random as rand
from Process import my_process

processes = []
saved = []

def make_rand_process(num, do_nothing = False):
    if not do_nothing:
        bursts = rand.sample(range(int(num*0.5), int(num*2.5)), num)
        arrivals = [rand.randint(0, num*2) for i in range(num)]
        periorities = rand.sample(range(num), num)
        
        for i in range(num):
            processes.append(my_process(i, bursts[i], arrivals[i], periorities[i]))
    else:
        pass
    # saved = processes.copy()
    return processes

if __name__ == "__main__":
    num = int(input('Enter number of processes: '))
    make_rand_process(num)
    for i in range(num):
        print(processes[i].get_info())